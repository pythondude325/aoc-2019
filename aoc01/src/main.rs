use std::io::{self, BufRead};

fn calculate_fuel_part_1(mass: i32) -> i32 {
    mass / 3 - 2
}

fn calculate_fuel_part_2(mass: i32) -> i32 {
    let fuel = calculate_fuel_part_1(mass);

    if fuel < 0 {
        0
    } else {
        fuel + calculate_fuel_part_2(fuel)
    }
}

fn main() {
    let stdin = io::stdin();
    let handle = stdin.lock();

    let total_fuel: i32 = handle.lines()
        .map(|line| line.unwrap().parse::<i32>().unwrap())
        .map(calculate_fuel_part_2) // Change this to calculate_fuel_part_1 for the part 1 solution
        .sum();

    println!("{}", total_fuel);
}
